<?php
return array(
    'login_table' => 'user',
    'profile_table' => 'user_profile',
    'login_url' => WEBROOT.'/',
    'logout_url' => WEBROOT.'/page/logout/',
    'home_url' => WEBROOT.'/stats/home/',
);